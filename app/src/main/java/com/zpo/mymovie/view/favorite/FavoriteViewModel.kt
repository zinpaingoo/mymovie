package com.zpo.mymovie.view.favorite

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.zpo.mymovie.repository.FavoriteMovieRepository

class FavoriteViewModel @ViewModelInject constructor(private val favoriteMovieRepository: FavoriteMovieRepository) : ViewModel(){

    val movies = favoriteMovieRepository.getFavoriteMovie()

}