package com.zpo.mymovie.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.zpo.mymovie.models.FavoriteMovie
import com.zpo.mymovie.room.FavoriteMovieDao

@Database(
    entities = [FavoriteMovie::class],
    version = 1
)
abstract class FavoriteMovieDatabase : RoomDatabase(){
    abstract fun getFavoriteMovieDao(): FavoriteMovieDao
}