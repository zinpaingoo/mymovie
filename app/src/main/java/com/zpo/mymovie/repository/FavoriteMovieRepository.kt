package com.zpo.mymovie.repository

import com.zpo.mymovie.models.FavoriteMovie
import com.zpo.mymovie.room.FavoriteMovieDao
import javax.inject.Inject

class FavoriteMovieRepository @Inject constructor(private val favoriteMovieDao: FavoriteMovieDao) {
    suspend fun checkMovie(id: String) = favoriteMovieDao.checkMovie(id)
    suspend fun addToFavorite(favoriteMovie: FavoriteMovie) = favoriteMovieDao.addToFavorite(favoriteMovie)
    suspend fun removeFromFavorite(id: String) = favoriteMovieDao.removeFromFavorite(id)

    fun getFavoriteMovie() =  favoriteMovieDao.getFavoriteMovie()


}