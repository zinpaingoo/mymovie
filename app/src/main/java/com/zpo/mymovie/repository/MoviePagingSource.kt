package com.zpo.mymovie.repository

import androidx.paging.PagingSource
import com.zpo.mymovie.api.MovieApi
import com.zpo.mymovie.models.Constants.Companion.STARTING_PAGE_INDEX
import com.zpo.mymovie.models.Movie
import retrofit2.HttpException
import java.io.IOException



class MoviePagingSource (
    private val movieApi: MovieApi,
    private val query: String?
): PagingSource<Int, Movie>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Movie> {


        return try {
            val position = params.key ?: STARTING_PAGE_INDEX
            val response = if (query!=null) movieApi.searchMovies(query,position) else movieApi.getNowPlayingMovies(position)
            val movies = response.results

            LoadResult.Page(
                data = movies,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position-1,
                nextKey = if (movies.isEmpty()) null else position+1
            )
        } catch (e: IOException){
            LoadResult.Error(e)
        } catch (e: HttpException){
            LoadResult.Error(e)
        }

    }
}