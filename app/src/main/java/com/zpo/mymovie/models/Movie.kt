package com.zpo.mymovie.models

import android.os.Parcelable
import com.zpo.mymovie.models.Constants.Companion.MOVIE_BASE_URL
import kotlinx.android.parcel.Parcelize



@Parcelize
data class Movie (
    val id: String,
    val overview : String?,
    val poster_path: String,
    val original_title: String
): Parcelable{
    val baseUrl get() = MOVIE_BASE_URL
}