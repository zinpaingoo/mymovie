package com.zpo.mymovie.models

import com.zpo.mymovie.BuildConfig

open class Constants {
   companion object{
       const val BASE_URL = "https://api.themoviedb.org/3/"
       const val MOVIE_BASE_URL = "https://image.tmdb.org/t/p/w500"
       const val API_KEY = BuildConfig.MOVIEDB_API_KEY
       const val STARTING_PAGE_INDEX = 1


   }
}
