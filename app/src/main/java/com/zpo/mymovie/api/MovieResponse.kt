package com.zpo.mymovie.api

import com.zpo.mymovie.models.Movie


data class MovieResponse (

    val results: List<Movie>
)

