package com.zpo.mymovie.api

import com.zpo.mymovie.BuildConfig
import com.zpo.mymovie.models.Constants.Companion.API_KEY

import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {


    @GET("movie/now_playing?api_key=$API_KEY")
    suspend fun getNowPlayingMovies(
        @Query("page") position: Int
    ): MovieResponse

    @GET("search/movie?api_key=$API_KEY")
    suspend fun searchMovies(
        @Query("query") query: String,
        @Query("page") page: Int
    ): MovieResponse
}