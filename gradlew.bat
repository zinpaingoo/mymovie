package com.zpo.huaweitutoappui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AppComponentFactory;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    String[] typeMusic = {"Pop Music","Rap Music","Rock Music","Metal Music","EDM Music","Blues Music","Jazz Music","Country Music"};
    int images[] = {
            R.drawable.m1,
            R.drawable.m2,
            R.drawable.m3,
            R.drawable.m4,
            R.drawable.m5,
            R.drawable.m2,
            R.drawable.m4,
            R.drawable.m1
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.lvCategory);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(layoutManager);
        Adapter adapter;
        adapter = new MyAdapter(this, typeMusic, images) {
        };
        recyclerView.setAdapter((RecyclerView.Adapter) adapter);
    }

    class MyAdapter extends ArrayAdapter<String>{

        Context context;
        String mType[];
        int mImages[];

        MyAdapter(Context context,String title[],int images[]){
            super(context,R.layout.category_album_items,R.id.albumName, title);
            this.context = context;
            this.mType = title;
            this.mImages = images;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

      